#ifndef MATH_H
#define MATH_H

#include <array>

#include <armadillo>

arma::mat33 make_R_x(double angle);
arma::mat33 make_R_y(double angle);
arma::mat33 make_R(double x_angle, double y_angle);
void map_to_array44(const arma::mat33 &R, const arma::vec3 &t,
                    std::array<double, 16> &arr);

#endif // MATH_H
