#include <iostream>
#include <unordered_map>
#include <algorithm>
#include <array>
#include <vector>
#include <fstream>

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

#include <armadillo>

#include "render.h"
#include "math.h"

namespace sf
{
  typedef Vector2<double> Vector2d;
}

struct MainCamera
{
  double x_angle;
  double y_angle;
  arma::vec3 c;
};

struct Camera
{
  unsigned id;
  std::array<double, 16> Rt;
};

double is_key_pressed(const std::unordered_map<int, double> &keys)
{
  auto key = std::find_if(keys.begin(), keys.end(),
                            [](const std::pair<int, double> &key)
  {
    return sf::Keyboard::isKeyPressed((sf::Keyboard::Key)key.first);
  });

  if (key != keys.end())
  {
    return key->second;
  }
  else
  {
    return 0;
  }
}

sf::Vector2d get_keyboard_delta(double sensitivity)
{
  return sf::Vector2d(is_key_pressed({{sf::Keyboard::Left, -sensitivity},
                                      {sf::Keyboard::Right, sensitivity}}),
                      is_key_pressed({{sf::Keyboard::Up, -sensitivity},
                                      {sf::Keyboard::Down, sensitivity}}));
}

sf::Vector2d get_mouse_delta(const sf::Window &window, double sensitivity)
{
  sf::Vector2i window_center = (sf::Vector2i)window.getSize() / 2;
  sf::Vector2i mouse_pos = sf::Mouse::getPosition(window);

  return (sf::Vector2d)(mouse_pos - window_center) * sensitivity;
}

MainCamera control(const MainCamera &main_camera, const sf::Window &window)
{
  sf::Vector2d keyboard_delta = get_keyboard_delta(0.01);
  sf::Vector2d mouse_delta = get_mouse_delta(window, 0.001);

  sf::Vector2i window_center = (sf::Vector2i)window.getSize() / 2;
  sf::Mouse::setPosition(window_center, window);

  double x_angle = main_camera.x_angle + keyboard_delta.y + mouse_delta.y;
  double y_angle = main_camera.y_angle + keyboard_delta.x + mouse_delta.x;

  arma::mat33 R = arma::inv(make_R(x_angle, y_angle));

  const double vel = 0.1;
  arma::vec3 relative_movement = arma::vec3({is_key_pressed({{sf::Keyboard::A, -vel},
                                                             {sf::Keyboard::D,  vel}}),
                                             0,
                                             is_key_pressed({{sf::Keyboard::W, -vel},
                                                             {sf::Keyboard::S,  vel}})});
  arma::vec3 absolute_movement = arma::vec3({0,
                                             is_key_pressed({{sf::Keyboard::Space, vel},
                                                             {sf::Keyboard::LControl, -vel}}),
                                             0});

  arma::vec3 c = main_camera.c + R * relative_movement + absolute_movement;

  return MainCamera({x_angle, y_angle, c});
}



void draw(MainCamera &main_camera, const std::vector<Camera> &cameras)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  arma::mat33 R = make_R(main_camera.x_angle, main_camera.y_angle);
  arma::vec3 t = -R * main_camera.c;
  std::array<double, 16> m = {0};
  map_to_array44(R, t, m);
  m[15] = 1;

  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixd(m.data());

  draw_grid(50);
  draw_coordinate_system();

  glPushMatrix();
  for (auto &camera: cameras)
  {
    glMultMatrixd(camera.Rt.data());
    draw_camera();
  }
  glPopMatrix();
}

bool poll_events(sf::Window &window)
{
  sf::Event event;
  if (window.pollEvent(event))
  {
    if (event.type == sf::Event::Resized)
    {
      glViewport(0, 0, event.size.width, event.size.height);

      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      gluPerspective(70, (double)event.size.width / event.size.height, 1, 100);
    }
    else if (event.type == sf::Event::Closed ||
             (event.type == sf::Event::KeyPressed &&
              event.key.code == sf::Keyboard::Escape))
    {
      return false;
    }

    return poll_events(window);
  }
  else
  {
    return true;
  }
}

void run(const MainCamera &main_camera, sf::Window &window,
         const std::vector<Camera> &cameras)
{
  if (poll_events(window)) // return false if quit
  {
    MainCamera new_main_camera = control(main_camera, window);
    draw(new_main_camera, cameras);

    window.display();

    run(new_main_camera, window, cameras);
  }
}

std::vector<Camera> load_cameras(const std::string &filename)
{
  std::vector<Camera> cameras;

  std::ifstream file(filename);
  while (file.good())
  {
    Camera camera;
    file >> camera.id;
    camera.Rt.fill(0);
    for (std::size_t i = 0; i < 12; ++i)
    {
      std::size_t row = i / 4;
      std::size_t col = i % 4;
      file >> camera.Rt[col * 4 + row];
    }
    std::cout << std::endl;
    camera.Rt[15] = 1;

    cameras.push_back(camera);
  }

  return cameras;
}

int main(int argc, char *argv[])
{
  sf::ContextSettings settings;
  settings.depthBits = 32;
  settings.stencilBits = 0;
  settings.antialiasingLevel = 0;
  settings.majorVersion = 2;
  settings.minorVersion = 1;

  sf::Window window(sf::VideoMode(800, 600), "Mocap viewer", sf::Style::Default, settings);
  window.setVerticalSyncEnabled(true);
  window.setMouseCursorVisible(false);

  glClearColor(0.0f, 0.3f, 0.6f, 1.0f);

  std::vector<Camera> cameras;
  if (argc > 1)
  {
    cameras = load_cameras(argv[1]);
    std::cout << cameras.size() << std::endl;
  }

  run(MainCamera({0, 0, arma::vec3({0, 2, 3})}), window, cameras);

  return 0;
}
