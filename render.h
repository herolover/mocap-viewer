#ifndef RENDER_H
#define RENDER_H

#include <cstddef>

void draw_camera();
void draw_coordinate_system();
void draw_grid(std::size_t plane_size);

#endif // RENDER_H
