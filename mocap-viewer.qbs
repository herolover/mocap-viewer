import qbs 1.0

Project {
    CppApplication {
        name: "mocap-viewer"

        files: [
            "main.cpp",
            "render.h",
            "render.cpp",
            "math.h",
            "math.cpp"
        ]

        cpp.cxxFlags: [
            "-std=c++11"
        ]

        cpp.linkerFlags: [
            "-larmadillo",
            "-lsfml-system",
            "-lsfml-window",
            "-lGL",
            "-lGLU"
        ]
    }
}
