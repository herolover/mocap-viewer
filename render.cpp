#include "render.h"

#include <SFML/OpenGL.hpp>


void draw_camera()
{
  glColor3d(0, 0, 0);
  glLineWidth(2.0f);
  /*
   * Plane
   */
  glBegin(GL_LINE_LOOP);
    glVertex3d(-1, -1, 1);
    glVertex3d( 1, -1, 1);
    glVertex3d( 1,  1, 1);
    glVertex3d(-1,  1, 1);
  glEnd();

  /*
   * Pyramid
   */
  glBegin(GL_LINES);
    glVertex3d(  0, 0, 0);
    glVertex3d(  1, 1, 1);

    glVertex3d(  0, 0, 0);
    glVertex3d( -1, 1, 1);

    glVertex3d( 0,  0, 0);
    glVertex3d(-1, -1, 1);

    glVertex3d( 0,  0, 0);
    glVertex3d( 1, -1, 1);
  glEnd();
}

void draw_coordinate_system()
{
  glLineWidth(3.0f);
  glBegin(GL_LINES);
    glColor3d(1, 0, 0);
    glVertex3d(0, 0, 0);
    glVertex3d(1, 0, 0);

    glColor3d(0, 1, 0);
    glVertex3d(0, 0, 0);
    glVertex3d(0, 1, 0);

    glColor3d(0, 0, 1);
    glVertex3d(0, 0, 0);
    glVertex3d(0, 0, 1);
  glEnd();
}

void draw_grid(std::size_t plane_size)
{
  glPushMatrix();
  glTranslated(plane_size * -0.5, 0, plane_size * -0.5);

  glLineWidth(1.0f);
  glColor3d(0.5, 0.5, 0.5);
  glBegin(GL_LINES);

  for (std::size_t i = 0; i <= plane_size; ++i)
  {
    glVertex3d(i, 0, 0);
    glVertex3d(i, 0, plane_size);

    glVertex3d(0, 0, i);
    glVertex3d(plane_size, 0, i);
  }
  glEnd();

  glPopMatrix();
}
