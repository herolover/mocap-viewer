#include "math.h"

#include <cassert>


arma::mat33 make_R_x(double angle)
{
  arma::mat33 R = arma::eye(3, 3);
  R(1, 1) = cos(angle);
  R(2, 2) = cos(angle);
  R(1, 2) = -sin(angle);
  R(2, 1) = sin(angle);

  return R;
}

arma::mat33 make_R_y(double angle)
{
  arma::mat33 R = arma::eye(3, 3);
  R(0, 0) = cos(angle);
  R(2, 2) = cos(angle);
  R(0, 2) = sin(angle);
  R(2, 0) = -sin(angle);

  return R;
}

arma::mat33 make_R(double x_angle, double y_angle)
{
  return make_R_x(x_angle) * make_R_y(y_angle);
}

void map_to_array44(const arma::mat33 &R, const arma::vec3 &t,
                    std::array<double, 16> &arr)
{
  const std::size_t cols = 4;

  for (std::size_t col = 0; col < 3; ++col)
  {
    for (std::size_t row = 0; row < 3; ++row)
    {
      arr[col * cols + row] = R(row, col);
    }
  }
  arr[3 * cols + 0] = t(0);
  arr[3 * cols + 1] = t(1);
  arr[3 * cols + 2] = t(2);
}
